---
id: setup
title: Setup developer environement
---

A brief description on how to setup your working directory to develop with us.

# Java environment
---

You need first to install [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and setup your JAVA_HOME and PATH environments variables.

## Intellij Idea

> Since we all use this IDE in Islands Wars team, we will only provides docs for it, but it's very similar for Eclipse & co.

Checkout our [guidelines](https://gitlab.com/islands-wars/guidelines) repository, download is_scheme.xml and import it in Intellij > Settings > Code Style, hit the gearing, Import Scheme as Intellij Code style XML.

You can now add our header to Settings > Editor > File and Code Templates > Includes.

You are now ready to clone / fork and develop.

# Rust environment
---
