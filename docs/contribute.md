---
id: contribute
title: How to contribute
---

When contributing to this group, please first discuss the change you wish to make via issue, email, discord before making a change.

Please note we have a code of conduct, described below, please follow it in all your interactions with the project.

# Who is a contributor
---

> If you help to improve Islands Wars code (by reporting or fixing bugs, or offering new idea), you're a contributor.

If you're looking for a way to write and contribute code, but you're not sure what to work on, simply check out our global project's Milestones or Issues pages for each project.
Or else you can join our discord to discuss about what to do.

# Pull request process
---

Make sure you are based on the latest `develop` branch.
- [The Zen of Python](http://legacy.python.org/dev/peps/pep-0020/)
- Follow our Code of Conduct
- Re-use dependency when reasonable
- Write unit-test on src/test/fr.islandswars.test.yourissues.ClassTest using JUnit 5
- Create or comment the associated Issues, following this pattern :

```
## Expected behavior


## Actual behavior


## Steps or code example to reproduce the problem

  1.
  1.
  1.

## Specifications

  - Version:
  - Platform:
  - Subsystem:
  
## Other informations, screenshot, etc
```

- Use a good git commit example

```
Short (50 chars or less) summary of changes

More detailed explanatory text, if necessary.  Wrap it to
about 72 characters or so.  In some contexts, the first
line is treated as the subject of an email and the rest of
the text as the body.  The blank line separating the
summary from the body is critical (unless you omit the body
entirely); tools like rebase can get confused if you run
the two together.

Further paragraphs come after blank lines.

  - Bullet points are okay, too

  - Typically a hyphen or asterisk is used for the bullet,
    preceded by a single space, with blank lines in
    between, but conventions vary here
```


# Code of conduct
---

We use git flow as branching model, you need to work on a dedicated features or hotfix branch.

## Java

Adopting the [Google Java Style](https://google.github.io/styleguide/javaguide.html) with the following changes:

```
3
    A source file consists of, in order:

      * Package statement
      * Import statements
      * Default generated header (Cf Setup dev environment)
      * Exactly one top-level class
      
      Exactly one blank line separates each section that is present.

4.2
    Our block indent is +4 characters

4.4
    Our line length is 200 characters.
```

We are a French team, therefore, we develop in English to make us understand by the majority.
Be concise when naming method, var, class according to Java standard naming conventions.
Always write documentation to public methods you add (in English), or everything that seems important, or TODO statement.

## Rust

TODO
