---
id: git-flow
title: Git flow cheatsheet
---

> git-flow are a set of git extensions to provide high-level repository operations for Vincent Driessen's branching model.

You can found [here](https://danielkummer.github.io/git-flow-cheatsheet/) a very user-friendly and efficient cheatsheet.

# Contributing
---

Simply checkout and pull the latest change from `develop` branch, and create new feature/hotfix branch according to the issues you work on :

Replace `feature` by `hotfix` if you're correcting a bug in the following example.
```bash
> git-flow feature start my-feature-branch-name
  //update/add/remove file
> git add .
> git commit -m ""
> git-flow feature publish my-feature-branch-name
```

And that's it, the project's developer will review your codes and then merge it.