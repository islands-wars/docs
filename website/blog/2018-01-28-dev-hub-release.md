---
title: Islands Wars dev hub
author: Xharos
authorURL: http://twitter.com/DevXharos
authorImage: /is/img/user/xharos.png
---

We are proud to release today our docusaurus documentation platform.

<!--truncate-->

You can found on this hub everything related to the Islands Wars public [code](https://gitlab.com/islands-wars) .
