/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');
const Container = CompLibrary.Container;
const siteConfig = require(`${process.cwd()}/siteConfig.js`);

class Users extends React.Component {
    render() {
        const showcase = siteConfig.users.map((user, i) => (
            <div>
                <a href={user.infoLink} key={i}>
                    <img src={user.image} title={user.caption} />
                </a>
                <h3>{user.caption}</h3>
            </div>
        ));

        return (
            <div className="mainContainer">
                <Container padding={['bottom', 'top']}>
                    <div className="showcaseSection">
                        <div className="prose">
                            <h1>Who's in ?</h1>
                            <p>This project is developed and maintained by the following people</p>
                        </div>
                        <div className="logos">{showcase}</div>
                    </div>
                </Container>
            </div>
        );
    }
}

module.exports = Users;
