/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');
const MarkdownBlock = CompLibrary.MarkdownBlock;
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

const siteConfig = require(`${process.cwd()}/siteConfig.js`);

const imgUrl = (img) => `${siteConfig.baseUrl}assets/images/${img}`;
const docUrl = (doc, language) => `${siteConfig.baseUrl}docs/${language ? language + '/' : ''}${doc}`
const pageUrl = (page, language) => `${siteConfig.baseUrl}${language ? language + '/' : ''}${page}`

class Button extends React.Component {
  render() {
    return (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={this.props.href} target={this.props.target}>
          {this.props.children}
        </a>
      </div>
    )
  }
}

Button.defaultProps = {
  target: '_self',
}

const SplashContainer = props => (
  <div className="homeContainer">
    <div className="homeSplashFade">
      <div className="wrapper homeWrapper">{props.children}</div>
    </div>
  </div>
);

const Logo = props => (
  <div className="projectLogo">
    <img src={props.img_src} />
  </div>
);

const ProjectTitle = props => (
  <h2 className="projectTitle">
    {siteConfig.title}
    <small>{siteConfig.tagline}</small>
  </h2>
);

const PromoSection = props => (
  <div className="section promoSection">
    <div className="promoRow">
      <div className="pluginRowBlock">{props.children}</div>
    </div>
  </div>
);

class HomeSplash extends React.Component {
  render() {
    let language = this.props.language || '';
    return (
      <SplashContainer>
        <Logo img_src={imgUrl('logo.svg')} />
        <div className="inner">
          <ProjectTitle />
          <PromoSection>
            <Button href="#">Documentation</Button>
            <Button href="#">Devblog</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

const Block = props => (
  <Container
    padding={['bottom', 'top']}
    id={props.id}
    background={props.background}>
    <GridBlock align={props.align || "center" } contents={props.children} layout={props.layout} />
  </Container>
);

const Features = props => (
  <Block layout="fourColumn">
    {[
      {
        content: 'You can find some documentation on our projects',
        image: imgUrl('icons/file_pen.png'),
        imageAlign: 'top',
        title: 'Documentation',
      },
      {
        content: 'You can also read articles from our devblog',
        image: imgUrl('icons/newspaper.png'),
        imageAlign: 'top',
        title: 'Devblog',
      },
    ]}
  </Block>
);

const Infos = props => (
  <Block background="light" align="justify">
    {[
      {
        content: 'If you think you\'re playing with a bug with our API or want to report an error on our documentation, open an issue on our <a href="#">issue tracker</a>.',
        image: imgUrl('logo.svg'),
        imageAlign: 'right',
        title: 'Getting Help',
      },
    ]}
  </Block>
);

const Showcase = props => {
  if ((siteConfig.users || []).length === 0)
    return null

  const showcase = siteConfig.users
    .filter(user => user.pinned)
    .map((user, i) => {
      return (
        <a href={user.infoLink} key={i}>
          <img src={user.image} title={user.caption} />
        </a>
      )
    })

  return (
    <div className="productShowcaseSection paddingBottom">
      <h2>{"Who's in ?"}</h2>
      <p>This project is developed and maintained by the following people</p>
      <div className="logos">{showcase}</div>
      <div className="more-users">
        <a className="button" href={pageUrl('team.html', props.language)}>
          More {siteConfig.title} Developers...
        </a>
      </div>
    </div>
  );
};

class Index extends React.Component {
  render() {
    const language = this.props.language || '';

    return (
      <div>
        <HomeSplash language={language} />
        <div className="mainContainer">
          <Features />
          <Infos />
          <Showcase language={language} />
        </div>
      </div>
    );
  }
}

module.exports = Index;
