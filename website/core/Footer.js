/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + 'docs/' + (language ? language + '/' : '') + doc;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? language + '/' : '') + doc;
  }

  render() {
    const currentYear = new Date().getFullYear();
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
            <h5>Docs</h5>
            <a href={this.docUrl('setup.html')}>
              Getting Started
            </a>
            <a href={this.docUrl('contribute.html')}>
              How to contribute
            </a>
          </div>
		  
		  <div>
            <h5>Api</h5>
            <a href={this.docUrl('plugin/setup.html')}>
              Spigot plugin
            </a>
            <a href={this.docUrl('sif/setup.html')}>
              Sign in Factory
            </a>
          </div>
		  
          <div>
            <h5>Community</h5>
            <a href={this.pageUrl('team.html')}>
              Team
            </a>
            <a href="https://discord.gg/vcGEnCY">Project Chat</a>
            <a href="https://twitter.com/Islands_Wars" target="_blank">
              Twitter
            </a>
          </div>
		  
          <div>
            <h5>More</h5>
            <a href={this.props.config.baseUrl + 'blog'}>Blog</a>
            <a href="https://gitlab.com/islands-wars" target="_blank">Gitlab</a>
          </div>
        </section>
      </footer>
    );
  }
}

module.exports = Footer;
