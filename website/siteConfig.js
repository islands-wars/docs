/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const users = [
    {
        caption: 'Xharos',
        image: 'assets/images/user/xharos.png',
        infoLink: 'https://twitter.com/DevXharos',
        pinned: true,
    },
    {
        caption: 'Vinetos',
        image: 'assets/images/user/vinetos.jpg',
        infoLink: 'https://twitter.com/Vinetos',
        pinned: true,
    },
    {
        caption: 'LightDiscord',
        image: 'assets/images/user/lightdiscord.jpg',
        infoLink: 'https://twitter.com/LightDiscord',
        pinned: true,
    },
    {
        caption: 'MrFeedthecookie',
        image: 'assets/images/user/cracfou.jpg',
        infoLink: 'https://twitter.com/cracfou',
        pinned: true,
    },
    {
        caption: 'NormanFeltz',
        image: 'assets/images/user/normanfeltz.jpg',
        infoLink: 'https://twitter.com/normanfeltz',
        pinned: true,
    },
]

const siteConfig = {
    title: 'Islands Wars',
    tagline: 'Documentation from Islands Wars\' developers',
    url: 'https://devs.islandswars.fr',
    baseUrl: '/',
    projectName: 'islands-wars',
    twitter: true,
    headerLinks: [
        {doc: 'setup', label: 'Docs'},
        {doc: 'api', label: 'API'},
        {page: 'team', label: 'Team'},
        {blog: true, label: 'Blog'},
    ],
    users,
    headerIcon: 'assets/images/logo.svg',
    footerIcon: 'assets/images/logo.svg',
    favicon: 'assets/images/favicon.png',
    colors: {
        primaryColor: '#689F38',
        secondaryColor: '#007e00',
    },
    copyright: `Copyright © ${new Date().getFullYear()} Islands Wars`,
    highlight: {
        theme: 'default',
    },
    // scripts: ['https://buttons.github.io/buttons.js'],
    repoUrl: 'https://gitlab.com/islands-wars/',
}

module.exports = siteConfig
